from django.shortcuts import render, redirect
from .models import User, Post, Reply, Work
from django.contrib import messages
import bcrypt

FOOTER = {
    'Created by Melissa Longenberger',
    'Thank you for visiting the Craft Hangout'
}

def index(request):
    context = {
        'footer': FOOTER,
    }
    return render(request, 'index.html', context)

def login(request):
    user = User.objects.filter(username = request.POST['username'])
    if user:
        userLogin = user[0]
        if bcrypt.checkpw(request.POST['password'].encode(), userLogin.password.encode()):
            request.session['user_id'] = userLogin.id
            return redirect('/dashboard/')
        messages.error(request, 'Invalid Credentials')
        return redirect('/')
    messages.error(request, 'That Username is not in our system, please register for an account')
    return redirect('/')

def signup(request):
    context = {
        'footer': FOOTER,
    }
    return render(request, 'register.html', context)

def register(request):
    if request.method == 'GET':
        return redirect('/register/')
    errors = User.objects.validate(request.POST)
    if errors:
        for err in errors.values():
            messages.error(request, err)
        return redirect('/register/')
    hashedPw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt()).decode()
    newUser = User.objects.create(
        firstName = request.POST['firstName'],
        lastName = request.POST['lastName'],
        email = request.POST['email'],
        username = request.POST['username'],
        password = hashedPw
    )
    request.session['user_id'] = newUser.id
    return redirect('/dashboard/')

def logout(request):
    request.session.clear()
    return redirect('/')

def dashboard(request):
    if 'user_id' not in request.session:
        return redirect('/')
    user = User.objects.get(id=request.session['user_id'])
    context = {
        'footer': FOOTER,
        'user': user,
    }
    print(user)
    return render(request, 'dashboard.html', context)

def editProfile(request, id):
    user = User.objects.get(id=request.session['user_id'])
    context = {
        'footer': FOOTER,
        'user': user,
    }
    return render(request, 'profile.html', context)

def updateProfile(request, id):
    toUpdate = User.objects.get(id=id)
    toUpdate.firstName = request.POST['firstName']
    toUpdate.lastName = request.POST['lastName']
    toUpdate.username = request.POST['username']
    toUpdate.email = request.POST['email']
    toUpdate.save()

    return redirect('/dashboard/')

def forum(request):
    pass

def createPost(request):
    pass

def viewPost(request):
    pass

def likePost(request):
    pass

def updatePost(request):
    pass

def deletePost(request):
    pass

def addReply(request):
    pass

def updateReply(request):
    pass

def deleteReply(request):
    pass

def work(request):
    pass

def addWork(request):
    pass

def viewWork(request):
    pass

def heartWork(request):
    pass

def updateWork(request):
    pass

def deleteWork(request):
    pass