from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('login/', views.login),
    path('register/', views.signup),
    path('logout/', views.logout),
    path('createUser/', views.register),
    path('dashboard/', views.dashboard),
    path('profile/<int:id>/viewUser/', views.editProfile),
    path('profile/<int:id>/updateUser/', views.updateProfile),
]