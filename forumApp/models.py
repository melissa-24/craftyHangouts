from django.db import models
import re

class UserManager(models.Manager):
    def validate(self, form):
        errors = {}
        if len(form['firstName']) < 2:
            errors['firstName'] = 'First name must be at least 2 characters'
        if len(form['lastName']) < 2:
            errors['lastName'] = 'Last name must be at least 2 characters'
        EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
        if not EMAIL_REGEX.match(form['email']):
            errors['email'] = 'Invalid Email Address'
        emailCheck = self.filter(email=form['email'])
        if emailCheck:
            errors['email'] = 'That Email Address is already in the system please log in or use a different Email Address'
        usernameCheck = self.filter(username=form['username'])
        if usernameCheck:
            errors['username'] = 'That username is already in use'
        if len(form['password']) < 6:
            errors['password'] = 'Password must be at least 6 characters long'
        if form['password'] != form['confirm']:
            errors['password'] = 'Passwords do not match'
        return errors

class User(models.Model):
    firstName=models.CharField(max_length=45)
    lastName=models.CharField(max_length=45)
    email=models.EmailField(unique=True)
    username=models.CharField(max_length=45)
    password=models.CharField(max_length=45)

    objects = UserManager()

class PostManager(models.Manager):
    def validate(self, form):
        errors = {}
        postNameCheck = self.filter(postTitle=form['postTitle'])
        if postNameCheck:
            errors['postTitle'] = 'Please use a different title for your post'
        if len(form['postText']) < 10:
            errors['postText'] = 'Please use at least 10 characters for your post'

class Post(models.Model):
    postTitle=models.CharField(max_length=100)
    postText=models.CharField(max_length=500)
    poster=models.ForeignKey(User, related_name='postMade', on_delete=models.CASCADE)
    postLikes=models.ManyToManyField(User, related_name='liked')
    postCreatedAt=models.DateTimeField(auto_now_add=True)
    postUpdatedAt=models.DateTimeField(auto_now=True)

    objects = PostManager()

class Reply(models.Model):
    reply=models.CharField(max_length=300)
    poster=models.ForeignKey(User, related_name='postReply', on_delete=models.CASCADE)
    message=models.ForeignKey(Post, related_name='replies', on_delete=models.CASCADE)
    replyCreatedAt=models.DateTimeField(auto_now_add=True)
    replyUpdatedAt=models.DateTimeField(auto_now=True)

class Work(models.Model):
    workTitle=models.CharField(max_length=45)
    workImg=models.CharField(max_length=255)
    workHeart=models.ManyToManyField(User, related_name='loved')
    user=models.ForeignKey(User, related_name='works', on_delete=models.CASCADE)
